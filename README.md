# hello-express_vol2

This project contains REST API with following features:

- Welcome endpoint
- Multiply endpoint

## Getting started

First install the dependencies:

```sh
npm i
```

Start the REST API service:

`node app.js`

Or is the latest NodeJS version (>=20):

`node --watch app.js`





